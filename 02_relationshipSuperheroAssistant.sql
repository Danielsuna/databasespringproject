ALTER TABLE assistant ADD COLUMN  hero_id int REFERENCES superhero(id);
ALTER TABLE assistant ADD FOREIGN KEY (hero_id) REFERENCES superhero(id);

